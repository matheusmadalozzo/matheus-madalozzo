package Editor_Imagem;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class Processamento_de_imagem {

	//Imagen atual carregada
	private BufferedImage imageActual; 


	//Retorna um objeto BufferedImage
	public BufferedImage abrirImagen(){ 
		//Vari�vel de retorno
		BufferedImage bmp=null; 
		//Janela de sele��o de arquivo
		JFileChooser selector=new JFileChooser(); 
		//T�tulo da janela
		selector.setDialogTitle("Selecione uma imagem"); 
		//Filtro de arquivos
		FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG & GIF & BMP", "jpg", "gif", "bmp"); 
		selector.setFileFilter(filtroImagen); 
		//Abre a janela de sele��o
		int flag=selector.showOpenDialog(null); 
		//Se clicar em abrir
		if(flag==JFileChooser.APPROVE_OPTION){ 
			try { 
				//Pega o arquivo selecionado 
				File imagemSelecionada = selector.getSelectedFile(); 
				//atribuimos � vari�vel bmp
				bmp = ImageIO.read(imagemSelecionada); 
			} catch (Exception e) { 
			} 

		} 
		//Atribuimos a imagem carregada para imageActual
		imageActual=bmp; 
		return bmp; 
	} 

	public BufferedImage escalaCinza(){ 
		//Vari�veis de armazenamento de pixels 
		int mediaPixel,colorSRGB; 
		Color colorAux; 

		//Percoreendo pixel a pixel
		for( int i = 0; i < imageActual.getWidth(); i++ ){ 
			for( int j = 0; j < imageActual.getHeight(); j++ ){ 
				//Pega a cor do pixel
				colorAux = new Color(this.imageActual.getRGB(i, j)); 
				//Calcula a m�dia dos canais (red, green, blue) 
				mediaPixel = (int)((colorAux.getRed()+colorAux.getGreen()+colorAux.getBlue())/3); 
				//Muda para o formato sRGB 
				colorSRGB = (mediaPixel << 16) | (mediaPixel << 8) | mediaPixel; 
				//Asignamos el nuevo valor al BufferedImage 
				imageActual.setRGB(i, j,colorSRGB); 
			} 
		} 
		//Retornamos la imagen 
		return imageActual; 
	} 
	
	public void contaCinza(){
		int[] valores = new int[256];
	    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		//int[] valores = new int[256];
		for( int i = 0; i < imageActual.getWidth(); i++ ){ 
			for( int j = 0; j < imageActual.getHeight(); j++ ){ 
				try{
					int gray = imageActual.getRGB(i, j) & 0xFF;
					valores[gray]++;
				}catch (Exception e) { 
				} 
			} 
		}
		for (int x = 0; x < valores.length; x++) {
            String v2 = String.valueOf(x);
            dataset.addValue(valores[x], "Cinza", v2);

        }
		JFreeChart chart = ChartFactory.createLineChart(
                "Histograma", // chart title
                "Pixels", // domain axis label
                "Quantidade", // range axis label
                dataset, // data
                PlotOrientation.VERTICAL, // orientation
                true, // include legend
                true, // tooltips?
                false // URLs?
                );
		
		BufferedImage img = imageActual;
		if (img != null) {			 
            final ChartPanel panel = new ChartPanel(chart);
            JFrame f = new JFrame("Histograma");
            JLabel jlblHist = new JLabel();

            f.setSize(800, 600);
            try {
                ChartUtilities.saveChartAsJPEG(new File("histograma.JPG"), chart, 800, 600);
                img = ImageIO.read(new File("histograma.JPG"));
                //ImageIcon icon = new ImageIcon(imageActual.resizeImage
                //        (img, jlblHist.getWidth(), jlblHist.getHeight()));
                jlblHist.setIcon(new ImageIcon(img));
            } catch (IOException ex) {
                
            }
            f.getContentPane().add(panel);
            f.setVisible(true);
        } else {
            JOptionPane.showConfirmDialog(null, "Selecione uma op��o de imagem");
        }
		
		//return valores;
	}
	
	public void fatiaCinza(){
		//BitPlane(imageActual);
	}
	
	
	public void salvar(){
		try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Salvar imagem...");
            FileFilter bmpFilter = new FileTypeFilter(".bmp", "Arquivos de Bitmap");
            FileFilter jpgFilter = new FileTypeFilter(".jpg", "JPEG");
            FileFilter pngFilter = new FileTypeFilter(".png", "PNG");
             
            fileChooser.addChoosableFileFilter(bmpFilter);
            fileChooser.addChoosableFileFilter(jpgFilter);
            fileChooser.addChoosableFileFilter(pngFilter);

            fileChooser.setMultiSelectionEnabled(false);
            
            int opcao = fileChooser.showSaveDialog(null);

            if (opcao == JFileChooser.APPROVE_OPTION) {
                /*File arquivo = fileChooser.getSelectedFile();
                ImageIO.write(imageActual, "bmp", new File(arquivo.getAbsolutePath() ));*/
                
                FileFilter selectedFileType = fileChooser.getFileFilter();
                // The getFileFilter should be renamed to getSelectedFileFilter, since it returns the selected one if there are more then one.
                if(selectedFileType.getDescription().equals(bmpFilter.getDescription())){
                	File arquivo = new File(fileChooser.getSelectedFile() + ".bmp");
                	ImageIO.write(imageActual, "bmp", new File(arquivo.getAbsolutePath() ));
                }
                else if(selectedFileType.getDescription().equals(jpgFilter.getDescription())){
                	File arquivo = new File(fileChooser.getSelectedFile() + ".jpg");
                	ImageIO.write(imageActual, "jpg", new File(arquivo.getAbsolutePath() ));
                }
                else if(selectedFileType.getDescription().equals(pngFilter.getDescription())){
                	File arquivo = new File(fileChooser.getSelectedFile() + ".png");
                	ImageIO.write(imageActual, "png", new File(arquivo.getAbsolutePath() ));
                }
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
	}
	

}
