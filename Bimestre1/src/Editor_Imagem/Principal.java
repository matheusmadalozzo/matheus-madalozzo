package Editor_Imagem;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

public class Principal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		Processamento_de_imagem ObjProcessamento=new Processamento_de_imagem();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Arquivo");
		menuBar.add(mnNewMenu);
		
		JMenu mnNewMenu_1 = new JMenu("Imagem");
		menuBar.add(mnNewMenu_1);
		
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("");
		
		JScrollPane scrollPane = new JScrollPane();
		this.contentPane.add(scrollPane, BorderLayout.CENTER);
		scrollPane.setViewportView(lblNewLabel);
		
				//JToolBar toolBar = new JToolBar();
				//scrollPane.setColumnHeaderView(toolBar);
				
				
				
						
				JMenuItem mntmNewMenuItem = new JMenuItem("Abrir");
				mntmNewMenuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						lblNewLabel.setIcon((Icon) new ImageIcon(ObjProcessamento.abrirImagen()));
						setBounds(100, 100, (lblNewLabel.getIcon().getIconWidth()+30), (lblNewLabel.getIcon().getIconHeight())+100);
					}
				});
				
				
				mnNewMenu.add(mntmNewMenuItem);
				
				
				JMenuItem mntmNewMenuItem_1 = new JMenuItem("Salvar");
				mntmNewMenuItem_1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ObjProcessamento.salvar();
					}
				});
				mnNewMenu.add(mntmNewMenuItem_1);
				

				
				JMenuItem mntmNewMenuItem_2 = new JMenuItem("Converter em tons de cinza");
				mntmNewMenuItem_2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						lblNewLabel.setIcon(new ImageIcon(ObjProcessamento.escalaCinza())); 
					}
				});
				mnNewMenu_1.add(mntmNewMenuItem_2);
				
				JMenuItem mntmNewMenuItem_3 = new JMenuItem("Fatiamento por planos de bits");
				mntmNewMenuItem_3.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ObjProcessamento.fatiaCinza();
					}
				});
				
				JMenuItem mntmHistogramaDeTons = new JMenuItem("Histograma de tons de cinza");
				mntmHistogramaDeTons.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ObjProcessamento.contaCinza();
					}
				});
				mnNewMenu_1.add(mntmHistogramaDeTons);
				mnNewMenu_1.add(mntmNewMenuItem_3);
		
		
		
	}

}
