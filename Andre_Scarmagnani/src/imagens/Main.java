package imagens;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class Main {

	private JFrame frame;
	private JLabel pictureLabel;
	BufferedImage img;
	EditaImagem imagem = new EditaImagem();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 841, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pictureLabel = new JLabel();
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JButton btnAbrir = new JButton("Abrir");
		btnAbrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pictureLabel.setIcon((Icon)new ImageIcon(imagem.abrir()));
				frame.getContentPane().add(pictureLabel);
				frame.pack();
			}
		});
		menuBar.add(btnAbrir);
		
		JButton btnSalvarImagem = new JButton("Salvar");
		btnSalvarImagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				imagem.salvar();
			}
		});
		menuBar.add(btnSalvarImagem);
		
		JButton btnConverterEmTons = new JButton("Imagem cinza");
		btnConverterEmTons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pictureLabel.setIcon((Icon)new ImageIcon(imagem.cinza()));
				frame.getContentPane().add(pictureLabel);
				frame.pack();
			}
		});
		
		JButton btnRestauraImagem = new JButton("Restaura imagem");
		btnRestauraImagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pictureLabel.setIcon((Icon)new ImageIcon(imagem.restaurar()));
				frame.getContentPane().add(pictureLabel);
				frame.pack();
			}
		});
		menuBar.add(btnRestauraImagem);
		menuBar.add(btnConverterEmTons);
		
		JButton btnGerarHistograma = new JButton("Histograma");
		btnGerarHistograma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				imagem.gerarHistograma();
			}
		});
		menuBar.add(btnGerarHistograma);
		
		JButton btnFatiamento = new JButton("Fatiamento");
		btnFatiamento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				imagem.fatiamento();
			}
		});
		menuBar.add(btnFatiamento);
		
		JButton btnMedia = new JButton("M\u00E9dia");
		btnMedia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int vizinhanca = 0;
				String str = (JOptionPane.showInputDialog("Informe o tamanho da mascara"));
				if(!str.isEmpty()){
					vizinhanca = Integer.parseInt(str);
				}
				pictureLabel.setIcon((Icon)new ImageIcon(imagem.media(vizinhanca)));
				frame.getContentPane().add(pictureLabel);
				frame.pack();
			}
		});
		menuBar.add(btnMedia);
		
		JButton btnMediana = new JButton("Mediana");
		btnMediana.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pictureLabel.setIcon((Icon)new ImageIcon(imagem.mediana()));
				frame.getContentPane().add(pictureLabel);
				frame.pack();
			}
		});
		menuBar.add(btnMediana);
		
		JButton btnLimiarizao = new JButton("Limiariza\u00E7\u00E3o");
		btnLimiarizao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int aux = 0;
				String str = (JOptionPane.showInputDialog("Informe o n�vel de corte"));
				if(!str.isEmpty()){
					aux = Integer.parseInt(str);
				}
				pictureLabel.setIcon((Icon)new ImageIcon(imagem.Limiarizacao(aux)));
				frame.getContentPane().add(pictureLabel);
				frame.pack();
			}
		});
		menuBar.add(btnLimiarizao);
	}

}
