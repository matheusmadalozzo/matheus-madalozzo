package imagens;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


public class EditaImagem {
	
	private BufferedImage img, restauraImg=null; 

	public BufferedImage abrir(){
		JFileChooser fc = new JFileChooser();
		FileNameExtensionFilter filtro = new FileNameExtensionFilter("JPG & GIF & BMP", "jpg", "gif", "bmp");
		fc.addChoosableFileFilter(filtro);
		int result = fc.showOpenDialog(null);
		if(result != JFileChooser.CANCEL_OPTION){
			try {
				img = ImageIO.read(fc.getSelectedFile());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		restauraImg=img;
		return img;
	}
	
	public BufferedImage restaurar(){
		img = restauraImg;
		return restauraImg;
	}
	
	public  void salvar(){
		JFileChooser salvandoArquivo = new JFileChooser();  
		int resultado = salvandoArquivo.showSaveDialog(null);  
		if (resultado != JFileChooser.APPROVE_OPTION) {
			return;  
		}  
		try {
			ImageIO.write(img, "PNG", salvandoArquivo.getSelectedFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public BufferedImage cinza(){
		int mediaPixel,colorSRGB; 
		Color colorAux; 
		for( int i = 0; i < img.getWidth(); i++ ){ 
			for( int j = 0; j < img.getHeight(); j++ ){
				colorAux = new Color(this.img.getRGB(i, j));
				mediaPixel = (int)((colorAux.getRed()+colorAux.getGreen()+colorAux.getBlue())/3);
				colorSRGB = (mediaPixel << 16) | (mediaPixel << 8) | mediaPixel;
				img.setRGB(i, j,colorSRGB); 
			} 
		} 
		return img;
	}
	
	public void gerarHistograma(){
		int[] valores = new int[256];
	    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		for( int i = 0; i < img.getWidth(); i++ ){ 
			for( int j = 0; j < img.getHeight(); j++ ){ 
				try{
					int gray = img.getRGB(i, j) & 0xFF;
					valores[gray]++;
				}catch (Exception e) { 
				} 
			} 
		}
		for (int x = 0; x < valores.length; x++) {
            String v2 = String.valueOf(x);
            dataset.addValue(valores[x], "Cinza", v2);

        }
		JFreeChart chart = ChartFactory.createLineChart(
                "Histograma",
                "Pixels",
                "Quantidade",
                dataset, // data
                PlotOrientation.VERTICAL,
                true, 
                true,
                false 
                );
		
		BufferedImage imagem = img;
		if (imagem != null) {			 
            final ChartPanel panel = new ChartPanel(chart);
            JFrame f = new JFrame("Histograma");
            JLabel jlblHist = new JLabel();

            f.setSize(800, 600);
            try {
                ChartUtilities.saveChartAsJPEG(new File("histograma.JPG"), chart, 800, 600);
                imagem = ImageIO.read(new File("histograma.JPG"));
                jlblHist.setIcon(new ImageIcon(imagem));
            } catch (IOException ex) {
                
            }
            f.getContentPane().add(panel);
            f.setVisible(true);
        } else {
            JOptionPane.showConfirmDialog(null, "Selecione uma op��o de imagem");
        }
	}
	
	public void fatiamento(){
		for(int i=0;i<8;i++){
			PlanoBit fatia = new PlanoBit(i, img);
			try {
				fatia.mostra();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public BufferedImage media(int vizinhanca) {
		BufferedImage imagem = img;
		Integer altura = imagem.getHeight();
		Integer largura = imagem.getWidth();
		int[][][] matrizImagem = new int[altura][largura][3];
		for (int j = 0; j < altura; j++) {
			for (int i = 0; i < largura; i++) {
				int rgb = imagem.getRGB(i, j);
				matrizImagem[j][i][0] = (rgb >> 16) & 255;
				matrizImagem[j][i][1] = (rgb >> 8) & 255;
				matrizImagem[j][i][2] = (rgb) & 255;
			}
		}
		imagem = mediaVizinhanca(vizinhanca, img, matrizImagem);
		img=imagem;
		return imagem;
	}
	
	public static BufferedImage mediaVizinhanca(int vizinhanca, BufferedImage imagemOriginal, int[][][] matrizImagem) {
		Integer altura = imagemOriginal.getHeight();
		Integer largura = imagemOriginal.getWidth();
		int[][][] matrizNovaImagem = new int[altura][largura][3];
		int somaR = 0;
		int somaG = 0;
		int somaB = 0;
		int contador = 0;
		int limiteLateral = vizinhanca / 2;
		int limSuplinha, limInflinha, limSupColuna, limInfColuna;
		for (int j = 0; j < altura; j++) {
			for (int i = 0; i < largura; i++) {
				limInflinha = j - limiteLateral;
				limSuplinha = j + limiteLateral;
				limInfColuna = i - limiteLateral;
				limSupColuna = i + limiteLateral;
				for (; limInflinha <= limSuplinha; limInflinha++, limInfColuna = i - limiteLateral) {
					for (; limInfColuna <= limSupColuna; limInfColuna++) {
						if ((limInflinha >= 0) && (limInfColuna >= 0) && (limInflinha < j) && (limInfColuna < i)) {
							somaR += matrizImagem[limInflinha][limInfColuna][0];
							somaG += matrizImagem[limInflinha][limInfColuna][1];
							somaB += matrizImagem[limInflinha][limInfColuna][2];
							contador++;
						}
					}
				}
				if (somaR != 0) {
					somaR = (int) Math.round(somaR / contador);
				}
				if (somaG != 0) {
					somaG = (int) Math.round(somaG / contador);
				}
				if (somaB != 0) {
					somaB = (int) Math.round(somaB / contador);
				}
				matrizNovaImagem[j][i][0] = somaR;
				matrizNovaImagem[j][i][1] = somaG;
				matrizNovaImagem[j][i][2] = somaB;
				somaR = somaG = somaB = contador = 0;
			}
		}
		BufferedImage novaImagem = new BufferedImage(largura, altura, imagemOriginal.getType());
		for (int j = 0; j < altura; j++) {
			for (int i = 0; i < largura; i++) {
				int r = ((matrizNovaImagem[j][i][0])) & 255;
				int g = ((matrizNovaImagem[j][i][1])) & 255;
				int b = (matrizNovaImagem[j][i][2]) & 255;
				int rgb = (r << 16) | (g << 8) | (b);
				novaImagem.setRGB(i, j, rgb);
			}
		}
		return novaImagem;
	}
	
	public BufferedImage mediana () {
		BufferedImage ima_out  = new BufferedImage(img.getWidth(),img.getHeight(),img.getType());
		Raster raster = img.getRaster();
		WritableRaster wraster = ima_out.getRaster();
		double valornr, valorng, valornb;
		int[] v = new int[9];
		for(int y=1; y<img.getHeight()-1; y++)
			for(int x=1; x<img.getWidth()-1; x++){
				LeJanela3x3(raster,v,x,y,0);
				valornr = CalcMediana(9,v);
				
				LeJanela3x3(raster,v,x,y,1);
				valorng = CalcMediana(9,v);
				
				LeJanela3x3(raster,v,x,y,2);
				valornb = CalcMediana(9,v);
				
				wraster.setSample(x,y,0,(int)(valornr+.5));
				wraster.setSample(x,y,1,(int)(valorng+.5));
				wraster.setSample(x,y,2,(int)(valornb+.5));
			}
		img=ima_out;
		return img;
		};
	    
	    public static void LeJanela3x3(Raster raster, int []v, int x, int y, int banda){
            v[0] = raster.getSample(x-1,y-1,banda);
            v[1] = raster.getSample(x  ,y-1,banda);
            v[2] = raster.getSample(x+1,y-1,banda);
            v[3] = raster.getSample(x-1,y  ,banda);
            v[4] = raster.getSample(x  ,y  ,banda);
            v[5] = raster.getSample(x+1,y  ,banda);
            v[6] = raster.getSample(x-1,y+1,banda);
            v[7] = raster.getSample(x  ,y+1,banda);
            v[8] = raster.getSample(x+1,y+1,banda);

            return;
        }
	    

	    public static double CalcMediana(int npts, int []v){
	    	int aux;
	    	for(int i=0; i<npts-1; i++)
	    		for(int j=i+1; j<npts; j++)
	    			if(v[i] > v[j]){
	    				aux = v[i]; v[i]=v[j]; v[j]=aux;
	    			}
	    	if((npts%2)==0)
	    		return((double)v[npts/2]);
	        else
	        	return((double)((v[npts/2]+v[npts/2+1])/2.));
	    }

	
	public BufferedImage Limiarizacao(int t){
		int BLACK = Color.BLACK.getRGB();     
        int WHITE = Color.WHITE.getRGB();
        
        BufferedImage output = new BufferedImage(img.getWidth(),     
                img.getHeight(), BufferedImage.TYPE_BYTE_GRAY); 
        
		for (int y = 0; y < img.getHeight(); y++)
			for (int x = 0; x < img.getWidth(); x++) {     
                Color pixel = new Color(img.getRGB(x, y));     
                output.setRGB(x, y, pixel.getRed() > t ? BLACK : WHITE);     
            }
		img = output;
		return img;
	}
}
