package imagens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;


public class PlanoBit {
	BufferedImage img,imagemTonsCinza;
	int bit;

	public PlanoBit(int bit, BufferedImage imagemCinza) {
		super();
		this.imagemTonsCinza = imagemCinza;
		this.bit = bit;
		this.img = new BufferedImage(imagemTonsCinza.getWidth(), imagemTonsCinza.getHeight(), imagemTonsCinza.getType());
	}

	public void mostra() throws IOException{
		for( int i = 0; i < imagemTonsCinza.getWidth(); i++ ){ 
			for( int j = 0; j < imagemTonsCinza.getHeight(); j++ ){ 
				try{
					//Color newPixel = new Color(imagemAtual.getRGB(i, j) & 128);
					//img.setRGB(i, j, imagemAtual.getRGB(i, j) & 128);//gray recebe o valor do pixel
					//System.out.println(imagemCinza.getRGB(i, j) & 128);
					if((imagemTonsCinza.getRGB(i, j) & 1<<bit)==0){
						img.setRGB(i, j, Color.BLACK.getRGB());
					}else{
						img.setRGB(i, j, Color.WHITE.getRGB());
					}
				}catch (Exception e) { 
					e.printStackTrace();
				} 
			} 
		}
		if (img != null) {			 
            JFrame f = new JFrame("Fatiamento Por Plano de Bit " + (bit+1));
            JPanel contentPane;
            f.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    		f.setBounds(100, 100, 450, 300);
            JMenuBar menuBar = new JMenuBar();
    		f.setJMenuBar(menuBar);
    		
    		contentPane = new JPanel();
    		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    		contentPane.setLayout(new BorderLayout(0, 0));
    		f.setContentPane(contentPane);
    		
            JLabel jlblHist = new JLabel();
            
            JScrollPane scrollPane = new JScrollPane();
    		contentPane.add(scrollPane, BorderLayout.CENTER);
    		scrollPane.setViewportView(jlblHist);
    		
            f.setSize(imagemTonsCinza.getWidth()+17, imagemTonsCinza.getHeight()+40);
			jlblHist.setIcon(new ImageIcon(img));
            f.getContentPane().add(jlblHist);
            f.setVisible(true);
            
            JButton btnSalvar = new JButton("Salvar");
            btnSalvar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					salvar();
				}
			});
            menuBar.add(btnSalvar);
        } else {
            JOptionPane.showConfirmDialog(null, "Selecione uma op��o de imagem");
        }
	}
	
	public void salvar(){
		JFileChooser salvandoArquivo = new JFileChooser();  
		int resultado = salvandoArquivo.showSaveDialog(null);  
		if (resultado != JFileChooser.APPROVE_OPTION) {
			return;  
		}  
		try {
			ImageIO.write(img, "PNG", salvandoArquivo.getSelectedFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
